import {Component} from '@angular/core';


@Component({
  selector: 'app-button',
  templateUrl: './button-demo.html',
  styleUrls: ['./button-demo.css']
})

export class ButtonDemo {
title = 'Button Components!';
isDisabled: boolean = false;
clickCounter: number = 0;
}
