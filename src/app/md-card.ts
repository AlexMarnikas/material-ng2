import {Component} from '@angular/core';


@Component({
  selector: 'app-mdcard',
  templateUrl: 'md-card.html',
  styleUrls: ['md-card.css']
})
export class MdcardComponent {
  title = 'MD CARD angular2 structure!';
}
