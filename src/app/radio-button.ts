import { Component } from '@angular/core';

@Component({
  selector: 'app-radio',
  templateUrl: 'radio-button.html',
  styleUrls: ['radio-button.css']
})
export class RadioComponent {
  title = 'Radio Button!';

  seasonOptions =  [
  new seasonClass('Spring'),
  new seasonClass('Winter'),
  new seasonClass('Easter'),
  new seasonClass('Autumn')
];


}

class seasonClass {
  constructor(
    public season: string) { }
}
