import { Component } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: 'toolbar.html',
  styleUrls: ['toolbar.css']
})
export class ToolbarComponent {
  title = 'Toolbar Component!';

  seasonOptions =  [
  new seasonClass('Spring'),
  new seasonClass('Winter'),
  new seasonClass('Easter'),
  new seasonClass('Autumn')
];


}

class seasonClass {
  constructor(
    public season: string) { }
}
