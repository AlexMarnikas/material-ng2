import {Component} from '@angular/core';


@Component({
  selector: 'app-gridlist',
  templateUrl: 'grid-list.html',
  styleUrls: ['grid-list.css'],
})
export class GridListComponent {

tiles: any[] = [
    {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
    {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
    {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'},
  ];

}
