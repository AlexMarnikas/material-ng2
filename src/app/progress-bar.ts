import { Component } from '@angular/core';

@Component({
  selector: 'app-bar',
  templateUrl: 'progress-bar.html',
  styleUrls: ['progress-bar.css']
})
export class BarComponent {
  title = 'Progress Bar!';

  progress = 0;
  constructor() {
    // Update the value for the progress-bar on an interval.
    setInterval(() => {
      this.progress = (this.progress + Math.floor(Math.random() * 4) + 1) % 100;
    }, 200);
  }

}
