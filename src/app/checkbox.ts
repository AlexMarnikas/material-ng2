import {Component} from '@angular/core';


@Component({
  selector: 'app-checkbox',
  templateUrl: 'checkbox.html',
  styleUrls: ['checkbox.css'],
})
export class CheckboxComponent {
  title = 'Checkbox Components!';
}
