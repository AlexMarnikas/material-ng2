import {Component} from '@angular/core';


@Component({
  selector: 'app-slider',
  templateUrl: 'slider.html',
  styleUrls: ['slider.css'],
})
export class SliderComponent {
  title = 'Slider Component!';
}
