import { Component } from '@angular/core';

@Component({

  selector: 'app-circle',
  templateUrl: 'progress-circle.html',
  styleUrls: ['progress-circle.css'],
})
export class ProgressCircleComponent {
  progressValue: number = 40;

  step(val: number) {
    this.progressValue += val;
  }

}
