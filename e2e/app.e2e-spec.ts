import { MaterialtryPage } from './app.po';

describe('materialtry App', function() {
  let page: MaterialtryPage;

  beforeEach(() => {
    page = new MaterialtryPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
